from django.shortcuts import render
from .forms import PostForm
from .models import Post
from django.shortcuts import redirect
from datetime import timezone
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
import requests
import json
import datetime

def index(request):
    return render(request, 'index.html')

def portfolio(request):
    return render(request, 'portfolio.html')

def register(request):
    return render(request, 'register.html')

def submissions(request):
    if (request.method == "POST"):
        database = Post.objects.all()
        for data in database:
            data.delete()
            return render(request, 'submissions.html')
    else:
        return render(request, 'submissions.html', {'submissionsList': Post.objects.all()})

def schedule(request):
    if request.method == "POST":
        form = PostForm(request.POST or None)
        if form.is_valid():
            date = request.POST['date']
            time = request.POST['time']
            activity = request.POST['activity']
            place = request.POST['place']
            category = request.POST['category']
            post = Post(date=date,time=time, activity=activity, place=place, category=category)
            post.save()
            return redirect('/submissions/')
    else:
        form = PostForm()
    return render(request, 'schedule.html', {'form' : form})

def books(request):
    return render(request, 'books.html')

def books_data(request):
    get_json_obj = requests.get('https://www.googleapis.com/books/v1/volumes?q=quilting')
    items = (get_json_obj.json())['items']
    return_data = {"booksData": items}
    json_data = json.dumps(return_data)
    return HttpResponse(json_data, content_type="application/json")
