from django.db import models
from datetime import date
from django import forms
import datetime

class Post(models.Model):
    date = models.DateTimeField()
    time = models.TimeField()
    activity = models.CharField(max_length=200)
    place = models.CharField(max_length=200)
    category = models.CharField(max_length=200)
