from django import forms
from .models import Post

class PostForm(forms.ModelForm):

    date = forms.DateTimeField(widget=forms.DateInput(attrs={'type':'date','class':'field'}),required=True)
    time = forms.CharField(widget=forms.TextInput(attrs={'type':'time','placeholder':'ex: 18:00'}),required=True)
    activity = forms.CharField(widget=forms.TextInput(attrs={'type':'text','placeholder':'Activty'}),required=True)
    place = forms.CharField(widget=forms.TextInput(attrs={'type':'text','placeholder':'Place'}),required=True)
    category = forms.CharField(widget=forms.TextInput(attrs={'type':'text','placeholder':'Category'}),required=True)

    class Meta:
        model = Post
        fields = ('date', 'time', 'activity', 'place', 'category')