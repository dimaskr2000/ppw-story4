from django.conf.urls import url
from .views import *
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^portfolio$', portfolio, name='portfolio'),
    url(r'^register$', register, name='register'),
    url(r'^schedule$', schedule, name='schedule'),
    url(r'^submissions$', submissions, name='submissions'),    
    url(r'^books$', books, name='books'),    
    url(r'^books_data$', books_data, name='books_data'),    
]
