from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .models import Post
from .forms import PostForm
from selenium import webdriver
import unittest
import datetime
import time
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
# Create your tests here.
class HelloTest(TestCase):

    def test_home_exists(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_home_using_index_func(self):
        function = resolve('/home/')
        self.assertEqual(function.func, index)

    def test_home_contains_title(self):
        response = Client().get('/home/')
        self.assertContains(response, 'Hello, my name is')

    def test_portfolio_contains_title(self):
        response = Client().get('/home/portfolio')
        self.assertContains(response, "Dimas Krissanto Rahmadi's Design Portfolio")
    
    def test_register_contains_title(self):
        response = Client().get('/home/register')
        self.assertContains(response, "Register to Dimas Krissanto Rahmadi's website")

    def test_schedule_contains_title(self):
        response = Client().get('/home/schedule')
        self.assertContains(response, "Schedule")

    def test_submissions_contains_title(self):
        response = Client().get('/home/submissions')
        self.assertContains(response, "Submitted Schedules")

    def test_form_validation(self):
        form = PostForm(data={'activity':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['activity'],['This field is required.'])

    def test_post_success_and_render_the_result(self):
        test = 'Test'
        response_post = Client().post('/home/schedule', {'date' : datetime.datetime.date(datetime.datetime.now()), 'time' : datetime.datetime.time(datetime.datetime.now()) , 'activity': test, 'place' : test, 'category' : test})
        self.assertEqual(response_post.status_code, 302)
        response = Client().get('/home/submissions')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_post_failed_and_render_the_result(self):
        test = 'Test'
        response_post = Client().post('/home/schedule', {'status': ''})
        self.assertEqual(response_post.status_code, 200)
        response = Client().get('/home/submissions')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)


class FunctionalTests(unittest.TestCase):

    def setUp(self):
        self.main_link = "http://localhost:8000/"
        options = Options()
        options.add_argument("--headless")
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-dev-shm-usage")
        self.browser = webdriver.Chrome("chromedriver", chrome_options=options)

    def tearDown(self):
        self.browser.quit()

